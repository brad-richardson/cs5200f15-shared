﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SharedObjects
{
    [DataContract]
    public class GameInfo
    {
        public enum StatusCode { NotInitialized=1, Initializing=2, Available=4,  Starting=8, InProgress=16, Ending=32, Complete=64, Cancelled=128 } ;
        [DataMember]
        public Int32 GameId { get; set; }
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public StatusCode Status { get; set; }
        [DataMember]
        public ProcessInfo GameManager { get; set; }
        [DataMember]
        public Int32 MinPlayers { get; set; }
        [DataMember]
        public Int32 MaxPlayers { get; set; }
        [DataMember]
        public Int32 StartingNumberOfPlayers { get; set; }
        [DataMember]
        public ProcessInfo[] CurrentProcesses { get; set; }
        [DataMember]
        public Int32 Winner { get; set; }

        public GameInfo()
        {
            SetupDefaults();
        }

        public GameInfo Clone()
        {
            GameInfo clone = MemberwiseClone() as GameInfo;
            clone.GameManager = GameManager.Clone();
            return clone;
        }

        private void SetupDefaults()
        {
            if (MinPlayers == 0) MinPlayers = 2;
            if (MaxPlayers == 0) MaxPlayers = 20;
            if ((int) Status == 0) Status = StatusCode.NotInitialized;
        }
    }
}
