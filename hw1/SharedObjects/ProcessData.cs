﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SharedObjects
{
    [DataContract]
    public class ProcessData
    {
        [DataMember]
        public Int32 GameId { get; set; }
        [DataMember]
        public Int32 ProcessId { get; set; }
        [DataMember]
        public ProcessInfo.ProcessType ProcessType { get; set; }
        [DataMember]
        public short LifePoints { get; set; }
        [DataMember]
        public short HitPoints { get; set; }
        [DataMember]
        public short NumberOfPennies { get; set; }
        [DataMember]
        public short NumberOfUnfilledBalloon { get; set; }
        [DataMember]
        public short NumberOfFilledBalloon { get; set; }
        [DataMember]
        public short NumberOfUnraisedUmbrellas { get; set; }
        [DataMember]
        public bool HasUmbrellaRaised { get; set; }
    }
}
