﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class RegisterGameRequest : Request
    {
        [DataMember]
        public GameInfo Game { get; set; }
    }
}
