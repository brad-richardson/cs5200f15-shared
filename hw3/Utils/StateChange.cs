﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class StateChange
    {
        public delegate void Handler();

        public enum ChangeType { NONE, ADDITION, UPDATE, MOVE, DELETION, ERRORCHANGED, SHUTDOWN };
        public ChangeType Type { get; set; }
        public object Subject { get; set; }
    }
}
