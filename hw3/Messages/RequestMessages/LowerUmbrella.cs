﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class LowerUmbrella : Request
    {
        [DataMember]
        public int UmbrellaId { get; set; }
    }
}
