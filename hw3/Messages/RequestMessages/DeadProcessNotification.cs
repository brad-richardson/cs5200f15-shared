﻿using System;
using System.Runtime.Serialization;

namespace Messages.RequestMessages
{
    [DataContract]
    public class DeadProcessNotification : Request
    {
        public int ProcessId { get; set; }
    }
}
